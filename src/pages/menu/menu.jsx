import React, { useEffect, useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
// import BasicPopover from './popup';
import AddMenuDialog from "./addMenuDialog";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import { Box, Button, IconButton } from "@mui/material";
import EditMenuDialog from "./editMenuDialog";
import DeleteMenuDialog from "./deleteMenuDialog";
import { useDispatch, useSelector } from "react-redux";
import { getAllMenu } from "../../actions/menu/menuAction";

// function createData(Food_Name, Food_Type, Image, Price, Edit, Delete) {
//   return { Food_Name, Food_Type, Image, Price, Edit, Delete };
// }

// const rows = [
//   createData("Pakhal", "Veg", "", 60),
//   createData("Chicken Biriyani", "Nonveg", "", 76),
//   createData("ThumsUp", "Cold drink", "", 66),
//   createData("Cupcake", "NonVeg", "", 67),
//   createData("Gingerbread", "Veg", "", 43),
// ];

export default function Menu() {
  const dispatch = useDispatch();
  const Token = sessionStorage.getItem("token");
  const allMenu = useSelector((state) => state.menu.all_menu_data)

  // add menu dialog start
  const [addOpen, setAddOpen] = React.useState(false);
  const handelAddDialog = () => {
    setAddOpen(!addOpen);
  }
  // add menu dialog end
  //edit table dialog start
  const [openEditMenu, setOpenEditMenu] = useState(false);
  const [editMenuData, setEditMenuData] = useState()
  const handleEditMenu = (row) => {
    setOpenEditMenu(!openEditMenu);
    setEditMenuData(row)
  };
  //edit table dialog end
  //Delete table dialog start
  const [openDeleteMenu, setOpenDeleteMenu] = useState(false);
  const [deleteMenuData, setDeleteMenuData] = useState();

  const handleDeleteMenu = (data) => {
    setDeleteMenuData(data);
    setOpenDeleteMenu(!openDeleteMenu);
  };
  //Delete table dialog end
  useEffect(() => {
    const obj = {
      token: Token,
    }
    dispatch(getAllMenu(obj)).then(() => {
      console.log("hiii data");

    })
  }, [Token, dispatch])

  return (
    <>
      <TableContainer component={Paper}>
        <Button
          onClick={handelAddDialog}
          variant="outlined"
          startIcon={<AddCircleOutlineIcon />}
        >
          Add New Menu
        </Button>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead sx={{ backgroundColor: "rgb(70 90 204)" }}>
            <TableRow>
              <TableCell sx={{ color: "white" }}>foodType</TableCell>
              <TableCell align="right" sx={{ color: "white" }}>
                foodNames
              </TableCell>

              <TableCell align="right" sx={{ color: "white" }}>
                foodImage
              </TableCell>
              <TableCell align="right" sx={{ color: "white" }}>
                foodPrice
              </TableCell><TableCell align="right" sx={{ color: "white" }}>
              Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {allMenu &&
              allMenu.map((row) => (
                <TableRow
                  key={row.foodType} /*  */
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.foodType}
                  </TableCell>
                  <TableCell align="right">{row.foodNames}</TableCell>
                  <TableCell align="right">
                  <Box>
                      <img
                        style={{ width: "8vw", height: "15vh" }}
                        alt="menu"
                        src={row.image}
                      />
                    </Box>

                  </TableCell>
                  <TableCell align="right">{row.price}</TableCell>
                  <TableCell align="right">
                    {
                      <IconButton onClick={() => handleEditMenu(row)} >
                        <EditIcon
                          sx={{ color: "blue", height: "30px", width: "30px" }}
                        />
                      </IconButton>
                    }
                    {
                      <IconButton onClick={() => handleDeleteMenu(row)} >
                        <DeleteIcon
                          sx={{ color: "red", height: "30px", width: "30px" }}
                        />
                      </IconButton>

                    }
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      <br></br>
      <AddMenuDialog open={addOpen} handelClose={handelAddDialog} />
      <EditMenuDialog open={openEditMenu} handelClose={handleEditMenu} editMenuData={editMenuData} />
      <DeleteMenuDialog open={openDeleteMenu} handleClose={handleDeleteMenu} deleteMenuData={deleteMenuData} />

    </>
  );
}
